package uk.co.paulbenn.poeninjaclient.deserializer;

import com.fasterxml.jackson.databind.node.JsonNodeType;
import lombok.Getter;

import java.io.IOException;

@Getter
public class JsonTypeMismatchException extends IOException {
    private final JsonNodeType expected;
    private final JsonNodeType actual;

    public JsonTypeMismatchException(JsonNodeType expected, JsonNodeType actual) {
        super("Expected JSON node type '" + expected + "', but found '" + actual + "'");
        this.expected = expected;
        this.actual = actual;
    }
}
