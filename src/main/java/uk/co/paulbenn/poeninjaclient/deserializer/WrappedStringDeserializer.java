package uk.co.paulbenn.poeninjaclient.deserializer;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.JsonNodeType;

import java.io.IOException;

public class WrappedStringDeserializer extends StdDeserializer<String> {

    public WrappedStringDeserializer() {
        this(String.class);
    }

    protected WrappedStringDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public String deserialize(JsonParser p, DeserializationContext ctx) throws IOException {
        JsonNode node = p.readValueAsTree();

        if (!node.isObject()) {
            throw new JsonTypeMismatchException(JsonNodeType.OBJECT, node.getNodeType());
        }

        if (node.size() != 1) {
            throw new IOException("Expecting OBJECT (1 property), got " + node.getNodeType() + " (" + node.size() + ")");
        }

        JsonNode firstChild = node.elements().next();

        if (!firstChild.isTextual()) {
            throw new JsonTypeMismatchException(JsonNodeType.STRING, firstChild.getNodeType());
        }

        return firstChild.asText();
    }
}
