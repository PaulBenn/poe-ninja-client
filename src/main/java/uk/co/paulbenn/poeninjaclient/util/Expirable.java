package uk.co.paulbenn.poeninjaclient.util;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import java.time.Instant;

@Getter
@Setter
public class Expirable {
    @JsonIgnore
    private Instant expiresAt;

    protected Expirable() {
        // prevent instantiation except by subclasses
    }
}
