package uk.co.paulbenn.poeninjaclient.util;

import java.net.URI;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

public class UriBuilder {

    private final Map<String, String> queryParameters = new HashMap<>();

    private String baseUrl;

    private String path;

    public UriBuilder baseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
        return this;
    }

    public UriBuilder path(String path) {
        this.path = path;
        return this;
    }

    public UriBuilder queryParameter(String key, String value) {
        this.queryParameters.put(key, value);
        return this;
    }

    public URI build() {
        StringBuilder uri = new StringBuilder(baseUrl + (path.startsWith("/") ? path : "/" + path));

        if (!queryParameters.isEmpty()) {
            uri.append("?");

            String query = queryParameters.entrySet()
                .stream()
                .map(e -> e.getKey() + "=" + e.getValue())
                .collect(Collectors.joining("&"));

            uri.append(query);
        }

        return URI.create(uri.toString());
    }
}
