package uk.co.paulbenn.poeninjaclient.model;

import lombok.Getter;

@Getter
public enum SortType {
    BY_EXPERIENCE("exp"),
    BY_DELVE_SOLO_DEPTH("depthsolo");

    private final String typeId;

    SortType(String typeId) {

        this.typeId = typeId;
    }
}
