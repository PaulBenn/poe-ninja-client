package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.EqualsAndHashCode;
import uk.co.paulbenn.poeninjaclient.deserializer.WrappedStringDeserializer;
import uk.co.paulbenn.poeninjaclient.entity.Language;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.time.Instant;
import java.util.List;
import java.util.Map;

@Data
@EqualsAndHashCode(callSuper = false)
public class BuildOverview extends Expirable {
    @JsonProperty("classNames")
    private List<String> classNames;
    @JsonProperty("classes")
    private List<Integer> classes;
    @JsonProperty("uniqueItems")
    private List<UniqueItem> uniqueItems;
    @JsonProperty("uniqueItemUse")
    private Map<Integer, List<Integer>> uniqueItemUse;
    @JsonProperty("activeSkills")
    private List<ActiveSkill> activeSkills;
    @JsonProperty("activeSkillUse")
    private Map<Integer, List<Integer>> activeSkillUse;
    @JsonProperty("allSkills")
    private List<ActiveSkill> allSkills;
    @JsonProperty("allSkillUse")
    private Map<Integer, List<Integer>> allSkillUse;
    @JsonProperty("keystones")
    private List<Keystone> keystones;
    @JsonProperty("keystoneUse")
    private Map<Integer, List<Integer>> keystoneUse;
    @JsonProperty("levels")
    private List<Integer> levels;
    @JsonProperty("life")
    private List<Integer> life;
    @JsonProperty("energyShield")
    private List<Integer> energyShield;
    @JsonProperty("weaponConfigurationTypes")
    @JsonDeserialize(contentUsing = WrappedStringDeserializer.class)
    private List<String> weaponConfigurationTypes;
    @JsonProperty("weaponConfigurationTypeUse")
    private List<Integer> weaponConfigurationTypeUse;
    @JsonProperty("names")
    private List<String> names;
    @JsonProperty("accounts")
    private List<String> accounts;
    @JsonProperty("ladderRanks")
    private List<Integer> ladderRanks;
    @JsonProperty("updatedUtc")
    private Instant updatedUtc;
    @JsonProperty("skillModes")
    @JsonDeserialize(contentUsing = WrappedStringDeserializer.class)
    private List<String> skillModes;
    @JsonProperty("skillModeUse")
    private Map<Integer, List<Integer>> skillModeUse;
    @JsonProperty("skillDetails")
    private List<SkillDetail> skillDetails;
    @JsonProperty("delveSolo")
    private List<Integer> delveSolo;
    @JsonProperty("language")
    private Language language;
    @JsonProperty("intervals")
    private List<Object> intervals; // TODO
    @JsonProperty("intervalNames")
    private List<Object> intervalNames; // TODO
    @JsonProperty("leagues")
    private List<Object> leagues; // TODO
    @JsonProperty("leagueNames")
    private List<Object> leagueNames; // TODO
    @JsonProperty("twitchAccounts")
    private List<Object> twitchAccounts; // TODO
    @JsonProperty("twitchNames")
    private List<Object> twitchNames; // TODO
    @JsonProperty("online")
    private List<Object> online; // TODO
    @JsonProperty("uniqueItemTooltips")
    private List<Boolean> uniqueItemTooltips;
    @JsonProperty("keystoneTooltips")
    private List<Boolean> keystoneTooltips;
}
