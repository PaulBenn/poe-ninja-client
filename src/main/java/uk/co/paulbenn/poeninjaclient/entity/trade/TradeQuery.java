package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeQuery {
    @JsonProperty("type")
    private TradeQueryType type;
    @JsonProperty("filters")
    private TradeQueryFilters filters;
}
