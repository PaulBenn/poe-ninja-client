package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeQueryMiscFilters {
    @JsonProperty("gem_level")
    private TradeQueryNumberFilter gemLevel;
    @JsonProperty("quality")
    private TradeQueryNumberFilter quality;
    @JsonProperty("corrupted")
    private TradeQueryBooleanFilter corrupted;
}
