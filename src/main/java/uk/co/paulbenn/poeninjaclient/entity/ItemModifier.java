package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ItemModifier {
    @JsonProperty("text")
    private String text;
    @JsonProperty("optional")
    private boolean optional;
}
