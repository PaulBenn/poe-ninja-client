package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Statistics {
    @JsonProperty("id")
    private int id;
    @JsonProperty("next_change_id")
    private String nextChangeId;
    @JsonProperty("api_bytes_downloaded")
    private long apiBytesDownloaded;
    @JsonProperty("stash_tabs_processed")
    private long stashTabsProcessed;
    @JsonProperty("api_calls")
    private long apiCalls;
    @JsonProperty("character_bytes_downloaded")
    private long characterBytesDownloaded;
    @JsonProperty("character_api_calls")
    private long characterApiCalls;
    @JsonProperty("ladder_bytes_downloaded")
    private long ladderBytesDownloaded;
    @JsonProperty("ladder_api_calls")
    private long ladderApiCalls;
    @JsonProperty("pob_characters_calculated")
    private long pobCharactersCalculated;
    @JsonProperty("oauth_flows")
    private long oAuthFlows;
}
