package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeQueryType {
    @JsonProperty("option")
    private String option;
    @JsonProperty("discriminator")
    private String discriminator;
}
