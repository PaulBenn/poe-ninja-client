package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CurrencyOverview extends Expirable {
    @JsonProperty("lines")
    private List<Currency> lines;
    @JsonProperty("currencyDetails")
    private List<CurrencyDetail> currencyDetails;
    @JsonProperty("language")
    private Language language;
}
