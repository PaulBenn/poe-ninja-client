package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeQueryFilters {
    @JsonProperty("misc_filters")
    private TradeQueryMiscFiltersWrapper miscFilters;
}
