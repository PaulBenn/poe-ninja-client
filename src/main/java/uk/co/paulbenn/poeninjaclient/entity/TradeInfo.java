package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeInfo {
    @JsonProperty("mod")
    private String mod;
    @JsonProperty("min")
    private int min;
    @JsonProperty("max")
    private int max;
}
