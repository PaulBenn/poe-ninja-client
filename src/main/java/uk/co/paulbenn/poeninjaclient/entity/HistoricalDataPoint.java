package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class HistoricalDataPoint {
    @JsonProperty("count")
    private int count;
    @JsonProperty("value")
    private double value;
    @JsonProperty("daysAgo")
    private int daysAgo;
}
