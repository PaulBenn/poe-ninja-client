package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeFilter {
    @JsonProperty("query")
    private TradeQuery query;
}
