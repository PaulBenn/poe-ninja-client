package uk.co.paulbenn.poeninjaclient.entity.trade;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class TradeQueryMiscFiltersWrapper {
    @JsonProperty("filters")
    private TradeQueryMiscFilters filters;
}
