package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.Map;

@Data
public class Language {
    @JsonProperty("name")
    private String name;
    @JsonProperty("translations")
    private Map<String, String> translations;
}
