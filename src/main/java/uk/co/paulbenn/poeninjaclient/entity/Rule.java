package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class Rule {
    @JsonProperty("section")
    private String section;
    @JsonProperty("rules")
    private List<String> rules;
}
