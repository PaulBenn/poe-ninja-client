package uk.co.paulbenn.poeninjaclient.entity;

import lombok.Data;
import lombok.EqualsAndHashCode;
import uk.co.paulbenn.poeninjaclient.util.ArrayWrapper;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.util.Arrays;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class ItemHistory extends Expirable implements ArrayWrapper<HistoricalDataPoint> {
    private List<HistoricalDataPoint> graphData;

    @Override
    public void setValues(HistoricalDataPoint[] array) {
        this.graphData = Arrays.asList(array);
    }
}
