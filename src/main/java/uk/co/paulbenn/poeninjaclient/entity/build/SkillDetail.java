package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
public class SkillDetail {
    @JsonProperty("name")
    private String name;
    @JsonProperty("supportGems")
    private SupportGems supportGems;
    @JsonProperty("dps")
    private Map<Integer, List<Integer>> dps;
}
