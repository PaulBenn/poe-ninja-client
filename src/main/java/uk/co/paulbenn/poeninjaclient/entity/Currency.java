package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Currency {
    @JsonProperty("currencyTypeName")
    private String currencyTypeName;
    @JsonProperty("pay")
    private PriceSnapshot pay;
    @JsonProperty("receive")
    private PriceSnapshot receive;
    @JsonProperty("paySparkLine")
    private SparkLine paySparkLine;
    @JsonProperty("receiveSparkLine")
    private SparkLine receiveSparkLine;
    @JsonProperty("chaosEquivalent")
    private double chaosEquivalent;
    @JsonProperty("lowConfidencePaySparkLine")
    private SparkLine lowConfidencePaySparkLine;
    @JsonProperty("lowConfidenceReceiveSparkLine")
    private SparkLine lowConfidenceReceiveSparkLine;
    @JsonProperty("detailsId")
    private String detailsId;
}
