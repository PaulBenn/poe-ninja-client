package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class Keystone {
    @JsonProperty("name")
    private String name;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("isKeystone")
    private String isKeystone;
    @JsonProperty("type")
    private String type;
}
