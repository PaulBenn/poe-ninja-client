package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class SparkLine {
    @JsonProperty("data")
    private double[] data;
    @JsonProperty("totalChange")
    private double totalChange;
}
