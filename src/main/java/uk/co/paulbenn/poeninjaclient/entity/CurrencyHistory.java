package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import uk.co.paulbenn.poeninjaclient.util.Expirable;

import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class CurrencyHistory extends Expirable {
    @JsonProperty("payCurrencyGraphData")
    private List<HistoricalDataPoint> payCurrencyGraphData;
    @JsonProperty("receiveCurrencyGraphData")
    private List<HistoricalDataPoint> receiveCurrencyGraphData;
}
