package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ActiveSkill {
    @JsonProperty("name")
    private String name;
    @JsonProperty("icon")
    private String icon;
    @JsonProperty("dpsName")
    private String dpsName;
}
