package uk.co.paulbenn.poeninjaclient.entity;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.time.Instant;

@Data
public class PriceSnapshot {
    @JsonProperty("id")
    private int id;
    @JsonProperty("league_id")
    private int leagueId;
    @JsonProperty("pay_currency_id")
    private int payCurrencyId;
    @JsonProperty("get_currency_id")
    private int getCurrencyId;
    @JsonProperty("sample_time_utc")
    private Instant sampleTimeUtc;
    @JsonProperty("count")
    private int count;
    @JsonProperty("value")
    private double value;
    @JsonProperty("data_point_count")
    private int dataPointCount;
    @JsonProperty("includes_secondary")
    private boolean includesSecondary;
    @JsonProperty("listing_count")
    private int listingCount;
}
