package uk.co.paulbenn.poeninjaclient.entity.build;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class UniqueItem {
    @JsonProperty("name")
    private String name;
    @JsonProperty("type")
    private String type;
}
