package uk.co.paulbenn.poeninjaclient;

import lombok.NonNull;
import uk.co.paulbenn.poeninjaclient.entity.CurrencyHistory;
import uk.co.paulbenn.poeninjaclient.entity.CurrencyOverview;
import uk.co.paulbenn.poeninjaclient.entity.HistoricalDataPoint;
import uk.co.paulbenn.poeninjaclient.entity.ItemHistory;
import uk.co.paulbenn.poeninjaclient.entity.ItemOverview;
import uk.co.paulbenn.poeninjaclient.entity.Rule;
import uk.co.paulbenn.poeninjaclient.entity.Statistics;
import uk.co.paulbenn.poeninjaclient.util.UriBuilder;

import java.util.Arrays;
import java.util.List;

public class PoeNinjaClient {

    private static final String DEFAULT_BASE_URL = "https://poe.ninja";

    protected final String baseUrl;

    protected final String league;

    private final DeserializingHttpClient httpClient;

    public PoeNinjaClient(String league) {
        this(league, new DeserializingHttpClient());
    }

    public PoeNinjaClient(String league, DeserializingHttpClient httpClient) {
        this(DEFAULT_BASE_URL, league, httpClient);
    }

    public PoeNinjaClient(String baseUrl, String league, DeserializingHttpClient httpClient) {
        this.baseUrl = baseUrl;
        this.league = league;
        this.httpClient = httpClient;
    }

    public CurrencyOverview getCurrencyOverview() {
        return getCurrencyOverview("Currency");
    }

    public CurrencyHistory getCurrencyHistory(String currencyId) {
        return getCurrencyHistory("Currency", currencyId);
    }

    public CurrencyOverview getFragmentOverview() {
        return getCurrencyOverview("Fragment");
    }

    public CurrencyHistory getFragmentHistory(String currencyId) {
        return getCurrencyHistory("Fragment", currencyId);
    }

    public ItemOverview getMapOverview() {
        return getItemOverview("Map");
    }

    public ItemHistory getMapHistory(String itemId) {
        return getItemHistory("Map", itemId);
    }

    public ItemOverview getDivinationCardOverview() {
        return getItemOverview("DivinationCard");
    }

    public ItemHistory getDivinationCardHistory(String itemId) {
        return getItemHistory("DivinationCard", itemId);
    }

    public ItemOverview getEssenceOverview() {
        return getItemOverview("Essence");
    }

    public ItemHistory getEssenceHistory(String itemId) {
        return getItemHistory("Essence", itemId);
    }

    public ItemOverview getBeastOverview() {
        return getItemOverview("Beast");
    }

    public ItemHistory getBeastHistory(String itemId) {
        return getItemHistory("Beast", itemId);
    }

    public ItemOverview getVialOverview() {
        return getItemOverview("Vial");
    }

    public ItemHistory getVialHistory(String itemId) {
        return getItemHistory("Vial", itemId);
    }

    public ItemOverview getFossilOverview() {
        return getItemOverview("Fossil");
    }

    public ItemHistory getFossilHistory(String itemId) {
        return getItemHistory("Fossil", itemId);
    }

    public ItemOverview getResonatorOverview() {
        return getItemOverview("Resonator");
    }

    public ItemHistory getResonatorHistory(String itemId) {
        return getItemHistory("Resonator", itemId);
    }

    public ItemOverview getScarabOverview() {
        return getItemOverview("Scarab");
    }

    public ItemHistory getScarabHistory(String itemId) {
        return getItemHistory("Scarab", itemId);
    }

    public ItemOverview getIncubatorOverview() {
        return getItemOverview("Incubator");
    }

    public ItemHistory getIncubatorHistory(String itemId) {
        return getItemHistory("Incubator", itemId);
    }

    public ItemOverview getOilOverview() {
        return getItemOverview("Oil");
    }

    public ItemHistory getOilHistory(String itemId) {
        return getItemHistory("Oil", itemId);
    }

    public ItemOverview getDeliriumOrbOverview() {
        return getItemOverview("DeliriumOrb");
    }

    public ItemHistory getDeliriumOrbHistory(String itemId) {
        return getItemHistory("DeliriumOrb", itemId);
    }

    public ItemOverview getInvitationOverview() {
        return getItemOverview("Invitation");
    }

    public ItemHistory getInvitationHistory(String itemId) {
        return getItemHistory("Invitation", itemId);
    }

    public ItemOverview getSkillGemOverview() {
        return getItemOverview("SkillGem");
    }

    public ItemHistory getSkillGemHistory(String itemId) {
        return getItemHistory("SkillGem", itemId);
    }

    public ItemOverview getBaseTypeOverview() {
        return getItemOverview("BaseType");
    }

    public ItemHistory getBaseTypeHistory(String itemId) {
        return getItemHistory("BaseType", itemId);
    }

    public ItemOverview getUniqueArmourOverview() {
        return getItemOverview("UniqueArmour");
    }

    public ItemHistory getUniqueArmourHistory(String itemId) {
        return getItemHistory("UniqueArmour", itemId);
    }

    public ItemOverview getUniqueWeaponOverview() {
        return getItemOverview("UniqueWeapon");
    }

    public ItemHistory getUniqueWeaponHistory(String itemId) {
        return getItemHistory("UniqueWeapon", itemId);
    }

    public ItemOverview getUniqueAccessoryOverview() {
        return getItemOverview("UniqueAccessory");
    }

    public ItemHistory getUniqueAccessoryHistory(String itemId) {
        return getItemHistory("UniqueAccessory", itemId);
    }

    public ItemOverview getUniqueJewelOverview() {
        return getItemOverview("UniqueJewel");
    }

    public ItemHistory getUniqueJewelHistory(String itemId) {
        return getItemHistory("UniqueJewel", itemId);
    }

    public ItemOverview getUniqueFlaskOverview() {
        return getItemOverview("UniqueFlask");
    }

    public ItemHistory getUniqueFlaskHistory(String itemId) {
        return getItemHistory("UniqueFlask", itemId);
    }

    public ItemOverview getUniqueMapOverview() {
        return getItemOverview("UniqueMap");
    }

    public ItemHistory getUniqueMapHistory(String itemId) {
        return getItemHistory("UniqueMap", itemId);
    }

    public Statistics getStats() {
        return httpClient.parse(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/GetStats")
                .build(),
            Statistics.class
        );
    }

    public List<Rule> getRules() {
        return Arrays.asList(httpClient.parse(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/GetRules")
                .build(),
            Rule[].class
        ));
    }

    protected CurrencyOverview getCurrencyOverview(@NonNull String type) {
        return httpClient.parseExpirable(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/CurrencyOverview")
                .queryParameter("league", league)
                .queryParameter("type", type)
                .build(),
            CurrencyOverview.class
        );
    }

    protected CurrencyHistory getCurrencyHistory(@NonNull String type, @NonNull String currencyId) {
        return httpClient.parseExpirable(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/CurrencyHistory")
                .queryParameter("league", league)
                .queryParameter("type", type)
                .queryParameter("currencyId", currencyId)
                .build(),
            CurrencyHistory.class
        );
    }

    protected ItemOverview getItemOverview(@NonNull String type) {
        return httpClient.parseExpirable(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/ItemOverview")
                .queryParameter("league", league)
                .queryParameter("type", type)
                .build(),
            ItemOverview.class
        );
    }

    protected ItemHistory getItemHistory(@NonNull String type, @NonNull String itemId) {
        return httpClient.parseExpirableArray(
            new UriBuilder()
                .baseUrl(baseUrl)
                .path("api/data/ItemHistory")
                .queryParameter("league", league)
                .queryParameter("type", type)
                .queryParameter("itemId", itemId)
                .build(),
            new ItemHistory(),
            HistoricalDataPoint.class
        );
    }
}
