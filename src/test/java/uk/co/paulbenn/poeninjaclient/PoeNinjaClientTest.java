package uk.co.paulbenn.poeninjaclient;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

class PoeNinjaClientTest {

    private PoeNinjaClient client;

    @BeforeEach
    void setUp() {
        client = new CachedPoeNinjaClient("Standard");
    }

    @Test
    void getCurrencyOverview() {
        assertDoesNotThrow(() -> client.getCurrencyOverview());
    }

    @Test
    void getFragmentOverview() {
        assertDoesNotThrow(() -> client.getFragmentOverview());
    }

    @Test
    void getMapOverview() {
        assertDoesNotThrow(() -> client.getMapOverview());
    }

    @Test
    void getDivinationCardOverview() {
        assertDoesNotThrow(() -> client.getDivinationCardOverview());
    }

    @Test
    void getEssenceOverview() {
        assertDoesNotThrow(() -> client.getEssenceOverview());
    }

    @Test
    void getBeastOverview() {
        assertDoesNotThrow(() -> client.getBeastOverview());
    }

    @Test
    void getVialOverview() {
        assertDoesNotThrow(() -> client.getVialOverview());
    }

    @Test
    void getFossilOverview() {
        assertDoesNotThrow(() -> client.getFossilOverview());
    }

    @Test
    void getResonatorOverview() {
        assertDoesNotThrow(() -> client.getResonatorOverview());
    }

    @Test
    void getScarabOverview() {
        assertDoesNotThrow(() -> client.getScarabOverview());
    }

    @Test
    void getIncubatorOverview() {
        assertDoesNotThrow(() -> client.getIncubatorOverview());
    }

    @Test
    void getOilOverview() {
        assertDoesNotThrow(() -> client.getOilOverview());
    }

    @Test
    void getDeliriumOrbOverview() {
        assertDoesNotThrow(() -> client.getDeliriumOrbOverview());
    }

    @Test
    void getInvitationOverview() {
        assertDoesNotThrow(() -> client.getInvitationOverview());
    }

    @Test
    void getSkillGemOverview() {
        assertDoesNotThrow(() -> client.getSkillGemOverview());
    }

    @Test
    void getBaseTypeOverview() {
        assertDoesNotThrow(() -> client.getBaseTypeOverview());
    }

    @Test
    void getUniqueArmourOverview() {
        assertDoesNotThrow(() -> client.getUniqueArmourOverview());
    }

    @Test
    void getUniqueWeaponOverview() {
        assertDoesNotThrow(() -> client.getUniqueWeaponOverview());
    }

    @Test
    void getUniqueAccessoryOverview() {
        assertDoesNotThrow(() -> client.getUniqueAccessoryOverview());
    }

    @Test
    void getUniqueJewelOverview() {
        assertDoesNotThrow(() -> client.getUniqueJewelOverview());
    }

    @Test
    void getUniqueFlaskOverview() {
        assertDoesNotThrow(() -> client.getUniqueFlaskOverview());
    }

    @Test
    void getUniqueMapOverview() {
        assertDoesNotThrow(() -> client.getUniqueMapOverview());
    }

    // The following history tests use an example available identifier for each item type

    @Test
    void getCurrencyHistory() {
        assertDoesNotThrow(() -> client.getCurrencyHistory("2"));
    }

    @Test
    void getFragmentHistory() {
        assertDoesNotThrow(() -> client.getFragmentHistory("82"));
    }

    @Test
    void getMapHistory() {
        assertDoesNotThrow(() -> client.getMapHistory("114423"));
    }

    @Test
    void getDivinationCardHistory() {
        assertDoesNotThrow(() -> client.getDivinationCardHistory("6"));
    }

    @Test
    void getEssenceHistory() {
        assertDoesNotThrow(() -> client.getEssenceHistory("369"));
    }

    @Test
    void getBeastHistory() {
        assertDoesNotThrow(() -> client.getBeastHistory("5111"));
    }

    @Test
    void getVialHistory() {
        assertDoesNotThrow(() -> client.getVialHistory("39818"));
    }

    @Test
    void getFossilHistory() {
        assertDoesNotThrow(() -> client.getFossilHistory("7634"));
    }

    @Test
    void getResonatorHistory() {
        assertDoesNotThrow(() -> client.getResonatorHistory("7635"));
    }

    @Test
    void getScarabHistory() {
        assertDoesNotThrow(() -> client.getScarabHistory("113836"));
    }

    @Test
    void getIncubatorHistory() {
        assertDoesNotThrow(() -> client.getIncubatorHistory("21674"));
    }

    @Test
    void getOilHistory() {
        assertDoesNotThrow(() -> client.getOilHistory("22523"));
    }

    @Test
    void getDeliriumOrbHistory() {
        assertDoesNotThrow(() -> client.getDeliriumOrbHistory("40903"));
    }

    @Test
    void getInvitationHistory() {
        assertDoesNotThrow(() -> client.getInvitationHistory("93814"));
    }

    @Test
    void getSkillGemHistory() {
        assertDoesNotThrow(() -> client.getSkillGemHistory("2598"));
    }

    @Test
    void getBaseTypeHistory() {
        assertDoesNotThrow(() -> client.getBaseTypeHistory("7837"));
    }

    @Test
    void getUniqueArmourHistory() {
        assertDoesNotThrow(() -> client.getUniqueArmourHistory("17"));
    }

    @Test
    void getUniqueWeaponHistory() {
        assertDoesNotThrow(() -> client.getUniqueWeaponHistory("12"));
    }

    @Test
    void getUniqueAccessoryHistory() {
        assertDoesNotThrow(() -> client.getUniqueAccessoryHistory("65"));
    }

    @Test
    void getUniqueJewelHistory() {
        assertDoesNotThrow(() -> client.getUniqueJewelHistory("58"));
    }

    @Test
    void getUniqueFlaskHistory() {
        assertDoesNotThrow(() -> client.getUniqueFlaskHistory("125"));
    }

    @Test
    void getUniqueMapHistory() {
        assertDoesNotThrow(() -> client.getUniqueMapHistory("2245"));
    }

    @Test
    void getStats() {
        assertDoesNotThrow(() -> client.getStats());
    }

    @Test
    void getRules() {
        assertDoesNotThrow(() -> client.getRules());
    }
}
