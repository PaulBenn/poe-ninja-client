package uk.co.paulbenn.poeninjaclient.util;

import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Arrays;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
public class LeaguesWrapper extends Expirable implements ArrayWrapper<League> {
    private List<League> leagues;

    @Override
    public void setValues(League[] array) {
        this.leagues = Arrays.asList(array);
    }
}
