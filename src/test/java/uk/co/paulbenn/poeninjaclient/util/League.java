package uk.co.paulbenn.poeninjaclient.util;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class League {
    private String id;
    private Instant endAt;
    private boolean event;
    private List<Object> rules;
}
