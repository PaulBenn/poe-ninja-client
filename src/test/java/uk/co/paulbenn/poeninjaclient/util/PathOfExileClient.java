package uk.co.paulbenn.poeninjaclient.util;

import uk.co.paulbenn.poeninjaclient.DeserializingHttpClient;

import java.net.URI;

public class PathOfExileClient {
    private final DeserializingHttpClient client = new DeserializingHttpClient();

    public String getCurrentTempLeague() {
        LeaguesWrapper wrapper = this.client.parseExpirableArray(
            URI.create("http://api.pathofexile.com/leagues"),
            new LeaguesWrapper(),
            League.class
        );

        return wrapper.getLeagues()
            .stream()
            .filter(l -> l.getEndAt() == null)
            .filter(l -> !l.isEvent())
            .filter(l -> l.getRules().isEmpty())
            .filter(l -> !"Standard".equals(l.getId()))
            .findFirst()
            .map(League::getId)
            .orElse("");
    }
}
